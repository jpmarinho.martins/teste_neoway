import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.dummy import DummyRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import StandardScaler, OneHotEncoder, LabelEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
import xgboost as xgb
import pickle
import argparse


def load_dataset(path):
    '''Carrega dataset de treino '''
    df = pd.read_csv(path)
    return df

def split(df):
    ''' Split de treino e teste '''
    X = df.drop('prob_V1_V2', axis=1)
    y = df.prob_V1_V2

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=42)

    return X_train, X_test, y_train, y_test

def mae_score(y_test, y_pred):
    ''' Funcao de score utilizada '''
    print('MAE:', mean_absolute_error(y_test, y_pred))
    return mean_absolute_error(y_test, y_pred)

def create_model(model_name=None):
    ''' Instancia um modelo baseado na escolha do usuário '''
    if model_name == 'xgboost':
        model = xgb.XGBRegressor()
    if model_name == 'randomforest':
        model = RandomForestRegressor()
    else:
        model = xgb.XGBRegressor()
    return model

def create_pipeline(model):
    ''' Cria a pipeline para o regressor'''
    #Pipeline para dados numéricos
    numeric_features = ['idade_v1', 'idade_v2', 'qt_filhos_v1', 'qt_filhos_v2', 'IMC_v1', 'IMC_v2']
    numeric_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='mean')), #Usando a mediana para valores missing
        ('scaler', StandardScaler())]) #Padronizando os inputs

    #Pipeline categórica
    categorical_features = ['grau', 'proximidade', 'estado_civil_v1', 'estado_civil_v2', 'transporte_mais_utilizado_v1', 'transporte_mais_utilizado_v2']
    categorical_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='constant', fill_value='missing')),
        ('onehot', OneHotEncoder(handle_unknown='ignore'))])
    
    #Pipeline de features categoricas mas que sao 0 e 1 D:
    strange_features = ['pratica_esportes_v1', 'pratica_esportes_v2', 'trabalha_v1', 'trabalha_v2', 'estuda_v1', 'estuda_v2']
    strange_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='constant', fill_value=-1)),
        ('strange', OneHotEncoder(handle_unknown='ignore'))])

    #Agregando as duas pipelines
    preprocessor = ColumnTransformer(
        transformers=[
            ('num', numeric_transformer, numeric_features),
            ('strn', strange_transformer, strange_features),
            ('cat', categorical_transformer, categorical_features)])

    #Pipeline final
    regressor = Pipeline(steps=[('preprocessor', preprocessor),
                          ('regressor', model)])
    return regressor

def create_model_pipeline(model_name):
    ''' Cria a pipeline do modelo '''
    model = create_model(model_name)
    pipeline = create_pipeline(model)

    return pipeline

def model_trainer(path_to_dataset, model_name):
    ''' Treina modelo '''
    
    print('Carregando dataset....\n')
    df = load_dataset(path_to_dataset)
    X_train, X_test, y_train, y_test = split(df)
    print('Dataset carregado e splitado!\n')

    pipeline = create_model_pipeline(model_name)

    print('Modelo em treinamento...')
    pipeline.fit(X_train, y_train)
    print('Modelo treinado!')

    y_pred = pipeline.predict(X_test)
    mae = mae_score(y_test, y_pred)

    return pipeline, mae

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_name", help="Nome do modelo a ser treinado (rf ou xgb)")
    parser.add_argument("--path_train_dataset", help="Caminho do dataset de treino")

    args = parser.parse_args()
    
    pipeline, mae = model_trainer(args.path_train_dataset, args.model_name)
    pickle.dump(pipeline, open('./models/'+args.model_name+'.sav','wb'))