from contaminacao_service import ContaminacaoRatePrediction
from train import model_trainer
import pickle
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--model_name", help="Nome do modelo a ser carregado (randomforest ou xgboost)")
args = parser.parse_args()

filename = './models/'+args.model_name+'.sav'
pipeline = pickle.load(open(filename, 'rb'))

bento_service = ContaminacaoRatePrediction()
bento_service.pack('ContaminacaoRatePrediction', pipeline)

saved_path = bento_service.save()#save_to_dir('./services')
print(saved_path)