import pandas as pd
import bentoml
from bentoml.frameworks.sklearn import SklearnModelArtifact
from bentoml.service.artifacts.common import PickleArtifact
from bentoml.handlers import DataframeHandler
from bentoml.adapters import DataframeInput, StringInput, FileInput
from io import BytesIO

@bentoml.artifacts([PickleArtifact('ContaminacaoRatePrediction')])
@bentoml.env(pip_packages=['scikit-learn', 'pandas', 'xgboost'])
class ContaminacaoRatePrediction(bentoml.BentoService):

    @bentoml.api(input=DataframeInput(orient='records'), batch=True)
    def predict_pandemic(self, df):
        """
        Predict contaminations rates with csv file
        Use in case of a pandemic
        The predictions will be saved in predictions folder
        """
        predictions = self.artifacts.ContaminacaoRatePrediction.predict(df)
        df['infection_rate'] = predictions

        df.to_csv('./predictions/preds.csv') #Saves file to csv

        return predictions

    @bentoml.api(input=DataframeInput(orient='records'), batch=True)
    def predict_single_case(self, df):
        """
        Predict contamination rate of a single case
        You better use that.
        """
        print(df)
        predictions = self.artifacts.ContaminacaoRatePrediction.predict(df)

        return predictions

    