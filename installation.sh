#!/bin/bash
#Para rodar este script é necessário Python3.6 ou maior e pip.

#################################################

echo "Criando virtualenv...."
virtualenv -p `which python3.6` contaminacao
echo "Feito!"

################################################

# abs_path = readlink -f contaminacao
# abs_path = "$abs_path/bin/activate"

# echo "Ativando o ambiente..."
# source $abs_path
# echo "Feito!"

###################################################

echo "Instalando requirements...."
./contaminacao/bin/pip install -r ./requirements.txt
echo "Feito!"

##################################################

echo "Ambiente ativado e pronto para uso!"