# Predição de Taxa de Contágio:

## Objetivo:
Este projeto foi desenvolvido para prever a taxa de contágio de uma pessoa (V1) para outra pessoa (V2) baseado nas características das mesmas.
Desta forma podemos ajudar a prever o quão a pandemia pode ser alastrar.

## Requisitos para Uso:
Para utilizar este projeto é aconselhável que se utilize um sistema Unix (de preferência Linux), pacote Virtualenvironment, Git e Python>=3.6.

## Como Instalar:
Para usar o projeto siga os passos abaixo:\
    1. Clone o repositório: *git clone https://gitlab.com/jpmarinho.martins/teste_neoway.git*\
    2. Entre na pasta: *cd teste_neoway*\
    3. Configure o uso do instalador de virtualenv: *chmod +x installation.sh*\
    4. Crie e instale o ambiente virtual: *./installation.sh*\
    5. Ative o ambiente (necessário ter virtualenv instalado): *source ./contaminacao/bin/activate*\
    6. Configure para predição em massa: *export BENTOML__APISERVER__DEFAULT_MAX_REQUEST_SIZE=200000000*\
    7. Crie o serviço de predição: *python create_bentoservice.py --model_name 'xgboost'* (O model_name pode ser 'randomforest' ou 'xgboost')\
    8. Sirva o modelo como uma API: *bentoml serve ContaminacaoRatePrediction:latest*\
    9. O modelo pode ser servido como um Docker também: *bentoml containerize ContaminacaoRatePrediction:latest -t contamination:v1*\
    9.1. Rode o docker: *docker run -p 5000:5000 contamination:v1*

## Uso da API:
A API será servida (tanto no docker quanto normalmente) através da localhost:5000.\
A API possui 2 endpoints principais:\
    1. Para realizar a predição em batch utilizando um arquivo .csv: localhost:5000/predict_pandemic\
        1.1. A chamada para esta API pode ser feita da seguinte forma:\
            ```
            curl -i \
            --header "Content-Type: text/csv" \
            --request POST \
            --data-binary @file.csv \
            localhost:5000/predict_pandemic
            ```\
        1.2. Para testar esse modo, o arquivo infer_df.csv foi criado. O caminho absoluto deve ser utilizado\
    2. Para realizar a predição de um caso isolado: localhost:5000/predict_single_case:\
        2.1. A chamada para esta API pode ser feita da seguinte forma:\
            ```
            curl -i \
            --header "Content-Type: application/json" \
            --request POST \
            --data '[{"grau":"trabalho", "proximidade":"visita_casual","idade_v1":24.0,"estado_civil_v1":"casado","qt_filhos_v1":0.0,"estuda_v1":0.0,"trabalha_v1":0.0,"pratica_esportes_v1":1.0,"transporte_mais_utilizado_v1":"publico","IMC_v1":25.378720020557605,"idade_v2":50.0,"estado_civil_v2":"casado","qt_filhos_v2":3.0,"estuda_v2":1.0,"trabalha_v2":0.0, "pratica_esportes_v2":0.0,"transporte_mais_utilizado_v2":"publico","IMC_v2":26.7320532813262}]'\
            localhost:5000/predict_single_case
            ```\
## Treino do modelo:

Para este projeto, foi-se escolhido dois modelos diferentes para inferência: XGBoostRegressor e RandomForestRegressor. A escolha se deu devido a boa performance de treino. 

Devido a boa distribuição de dados e a baixa incidência de outliers a métrica de controle escolhida foi MAE(Mean Absolute Error).

O modelo pode ser treinado através do seguinte comando:
```
python train.py --model_name 'xgboost' --path_train_dataset './train_dataset.csv'
```

Paramêtros:


**--model_name**: nome do modelo a ser treinado, pode ser: 'xgboost' ou 'randomforest'


**--path_train_dataset**: caminho para o dataset

Os modelos treinados são salvos na pasta: **models/model_name.sav**

### Features de Treino e Target:

O modelo foi feito utilizando-se as seguintes features:


grau,proximidade,idade_v1,estado_civil_v1,qt_filhos_v1,estuda_v1,trabalha_v1,pratica_esportes_v1,transporte_mais_utilizado_v1,IMC_v1,idade_v2,estado_civil_v2,qt_filhos_v2,estuda_v2,trabalha_v2,pratica_esportes_v2,transporte_mais_utilizado_v2,IMC_v2

O target foi:

prob_V1_V2

## EDA:

Para facilitar a visualiação de dados os seguintes reports foram gerados em HTML:

* Conexões_EDA: Report dos dados de transmissão;
* Individuos_EDA: Report dos individuos;
* Join: Junção dos dados de conexões e individuos.
